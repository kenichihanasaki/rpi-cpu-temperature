/* 
 * File:   main.cpp
 * Author: admin
 * 
 * パイプを使ったコマンド呼び出しによるCPU温度の取得を行います。
 * 
 * Created on 2015/02/27, 20:18
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
 * 
 */
int main(int argc, char** argv) {
    
    char buffer[1024];
    memset(buffer, 0, sizeof(buffer));
    
    const char* command = "cat /sys/class/thermal/thermal_zone0/temp";
    
    FILE* fp = popen(command, "r");
    
    fgets(buffer, sizeof(buffer), fp);
    pclose(fp);
    
    printf("cpu-temperature %s\n", buffer);
    
    return 0;
}

